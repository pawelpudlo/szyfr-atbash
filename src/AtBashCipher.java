import java.lang.reflect.GenericDeclaration;

class AtBashCipher implements Cipher {

    private Character[] leeters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    private int lettersSize = leeters.length-1;
    private int x=0;

    @Override
    public String decode(String message) {

        return encode(message);

    }

    @Override
    public String encode(String message) {
        int size = message.length();
        String mess = message.toUpperCase();
        String encodeMessage = "";

        if(size == 0){
            return "";
        }

        for(int i=0; i<size; i++){
            if(mess.charAt(i) == ' '){
                encodeMessage += " ";
                continue;
            }
            x=0;
            for(int j=0; j<=lettersSize; j++){

                if(mess.charAt(i) == leeters[j]){
                    if(x<13){
                        encodeMessage += leeters[lettersSize-x];
                    }else{
                        encodeMessage += leeters[13-(x-12)];
                    }
                    continue;
                }else{
                    x++;
                }
            }
        }
        encodeMessage = encodeMessage.toLowerCase();
        String encodeMessageGoodText="";

        for(int i=0; i<size;i++){
            if(i!=0){
                encodeMessageGoodText += encodeMessage.charAt(i);
            }else{
                String help = "" + encodeMessage.charAt(i);
                encodeMessageGoodText += help.toUpperCase();
            }
        }

        return encodeMessageGoodText;
    }
}

